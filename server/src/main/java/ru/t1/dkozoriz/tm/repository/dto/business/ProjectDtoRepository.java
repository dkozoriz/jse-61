package ru.t1.dkozoriz.tm.repository.dto.business;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;


@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends BusinessDtoRepository<ProjectDto> {

}