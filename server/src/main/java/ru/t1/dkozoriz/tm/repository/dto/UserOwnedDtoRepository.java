package ru.t1.dkozoriz.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDto;

import java.util.List;

@NoRepositoryBean
public interface  UserOwnedDtoRepository<T extends UserOwnedModelDto> extends AbstractDtoRepository<T> {

    @NotNull
    List<T> findAllByUserId(@NotNull String userId);

    @Nullable
    T findByIdAndUserId(@NotNull String id, @NotNull String userId);

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    void deleteByIdAndUserId(@NotNull String id, @NotNull String userId);

}